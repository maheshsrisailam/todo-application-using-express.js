
const express = require('express')

const app = express()

app.use(express.json())
app.use(express.urlencoded({extended: false}))

app.use('/todos', require('./routes/apis/todos'))
app.use((request,response,next) => {
    const error = new Error("Not Found")
    error.status = 404
    next(error)
})

app.use((error,request,response,next) => {
    response.status(error.status || 500).json({ERROR : error.message})
})

const PORT = process.env.PORT || 3000

app.listen(PORT, () => console.log(`Server started listening on port : ${PORT}`))
