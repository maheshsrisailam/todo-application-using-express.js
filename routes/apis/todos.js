const express = require('express')
const uuid = require('uuid')
const router = express.Router()
const todosData = require('../../todosData')


// Getting all todos
router.get('/',(request,response) => {
    try {
        response.json(todosData)
    } catch (error) {
        response.status(400).json({ERROR: `${error.message}`})
    }
})

//Getting a specific todo
router.get('/:id', (request,response) => {
    try {
        const isTodoFound = todosData.find((eachTodo) => eachTodo.id === parseInt(request.params.id))
        if (isTodoFound) {
            response.json(isTodoFound)
        } else {
            response.status(400).json({message: `The todo with ID : ${request.params.id} is not found.`})
        }
    } catch (error) {
        response.status(400).json({ERROR: `${error.message}`})
    }
})

//Creating a new todo 

router.post('/', (request,response) => {
    try {
        const newTodo = {
            id : uuid.v4(),
            todo : request.body.todo,
            completed : request.body.completed,
            userId : request.body.userId
        }
    
        if (!newTodo.todo || !newTodo.completed || !newTodo.userId) {
            return response.status(400).json({message:"Please give values to todo, completed and userId."})
        } else {
            todosData.push(newTodo)
            response.send(newTodo)
        }
    } catch (error) {
        response.status(400).json({ERROR:`${error.message}`})
    }
})

//Updating entire todo

router.put('/:id', (request,response) => {
    try {
        const isTodoFound = todosData.find((eachTodo) => eachTodo.id === parseInt(request.params.id))
        if (isTodoFound) {
            const dataReceivedFromRequest = request.body
            todosData.forEach((eachTodo) => {
                if (eachTodo.id === parseInt(request.params.id)) {
                    eachTodo.todo = dataReceivedFromRequest.todo
                    eachTodo.completed = dataReceivedFromRequest.completed
                    eachTodo.userId = dataReceivedFromRequest.userId

                    if ((eachTodo.todo !== undefined) && (eachTodo.completed !== undefined) && (eachTodo.userId !== undefined)) {
                        response.json({message : "Todo is updated completely", eachTodo})
                    } else {
                        response.status(400).json({message:"Please all the fields."})
                    }
                }
            })
            
        } else {
            response.status(400).json({message: `The todo with ID : ${request.params.id} is not found.`})
        }
    } catch (error) {
        response.status(400).json({ERROR: `${error.message}`})
    }
})

//Updating a todo partially

router.patch('/:id', (request,response) => {
    try {
        const isTodoFound = todosData.filter((eachTodo) => eachTodo.id === parseInt(request.params.id))
        if (isTodoFound) {
            const updatedTodo = request.body
            todosData.forEach((eachTodo) => {
                if (eachTodo.id === parseInt(request.params.id)) {
                    eachTodo.todo = updatedTodo.todo ? updatedTodo.todo : eachTodo.todo
                    eachTodo.completed = updatedTodo.completed ? updatedTodo.completed : eachTodo.completed
                    eachTodo.userId = updatedTodo.userId ? updatedTodo.userId : eachTodo.userId

                    response.json({message : "Todo is updated partially", eachTodo})
                }
            })
        } else {
            response.status(400).json({message: `The todo with ID : ${request.params.id} is not found.`})
        }
    } catch (error) {
        response.status(400).json({ERROR: `${error.message}`})
    }
})


//Deleting a specific todo

router.delete('/:id', (request,response) => {
    try {
        const isTodoFound = todosData.find((eachTodo)=>eachTodo.id === parseInt(request.params.id))
        const todosAfterDelete = todosData.filter((eachTodo) => eachTodo.id !== parseInt(request.params.id))
        if (isTodoFound) {
            response.json({message: `Todo with ID: ${request.params.id} is deleted successfully`,todos:todosAfterDelete})
        } else {
            response.status(400).json({message: `The todo with ID : ${request.params.id} is not found.`})
        }
    } catch (error) {
        response.status(400).json({ERROR:`${error.message}`})
    }
})


module.exports = router